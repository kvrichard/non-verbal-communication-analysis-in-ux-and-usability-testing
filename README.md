# Non-verbal communication analysis in UX and Usability testing
This repository contains all my research & applications in non-verbal language in the UX & Usability testing fields.
All the documentation are written in French only for now. An English version will come soon after the study is done.

## Project structure
- `roadmap.md` represent the actual study & writing process
- `Non-verbal communication analysis in UX and Usability testing.md` contains the main study & testing
- `web/` directory contains output of the files for the web, meaning HTML formatted

## File output
This project use CodeKit in order to generate **HTML** & **CSS** files. CodeKit compiles Markdown using the [MultiMarkdown](https://fletcher.github.io/MultiMarkdown-4/) library, which allows *footnotes* links and many other great options for publication documents. This may cause some display issue with the default Gitlab Markdown preview. Please refer to the current `web/Non-verbal communication analysis in UX and Usability testing.html` output for the proper display.

## Release version
Here you can find the latest release of this research. Note that this project is still a **Work In Progress** !

[Releases &rarr;](https://gitlab.com/kvrichard/non-verbal-communication-analysis-in-ux-and-usability-testing/tags)

## Changelog
- **v 1.0.3**
    + Add new items to "Conditions & recommandations" list
    + Change "Expérimentation" section structure
    + General text improvements
    + Move "Conditions & recommandations" under the "Expérimentation" section

## TODO
- **Experimentation**
    + Environnement
    + Participants profile
    + Proceedings
    + Testing
    + Results
- **Conclusion**
