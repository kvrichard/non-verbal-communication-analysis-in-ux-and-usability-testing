---
lang: fr-FR
charset: UTF-8
title: Article Roadmap
author: Kevin Richard
email: kvrichard@gmail.com
date: 20 June 2016
CSS: css/style.css
---
# Roadmap
1. **Intro**
    + Contexte d'analyse - **DONE**
    + Historique - **DONE**
        * UX Design & testes d'utilisabilité - **DONE**
            - Qu'est-ce que l'UX ? - **DONE**
            - Qu'est-ce qu'un teste d'utilisabilité ? - **DONE**
            - Utilisation concrète des données en UX Design - **DONE**
        * Communication non verbale - **DONE**
            - Qu'est-ce que le langage non verbal ? - **DONE**
                + Concept - **DONE**
                + Les principaux courants scientifiques - **DONE**
                + La méthode Bodysystemics - **DONE**
2. **Théorie**
    + Observation - **DONE**
    + Problèmes & limitations - **DONE**
    + Idée / concept / sujet - **DONE**
    + Exemple d'application - **DONE**
3. **Expérimentation**
    + Cadre/environnement - **TODO**
    + Profile des participants - **TODO**
    + Procédures - **TODO**
    + Résultats - **TODO**
4. **Conclusion** - **TODO**
5. **Références** - **WIP**
