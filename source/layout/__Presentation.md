<div class="Presentation">
    <div class="Overlay Overlay--cover Overlay--blur" style="background-image: url('img/photo-1474403078171-7f199e9d1335.jpeg');"></div>
    <div class="Overlay Overlay--transparency Overlay--transparency"></div>
    <div class="Wrapper Wrapper--textCentered Wrapper--textWhite Wrapper--textShadow--high">
        <h1> Analyse du langage non verbal en UX et tests d'utilisabilité </h1>
        <h2> Recherches sur l'application de méthodes &amp; études de cas </h2>
    </div>
</div>
