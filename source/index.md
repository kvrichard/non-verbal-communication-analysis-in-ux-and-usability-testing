{{partials/__meta.md}}
{{layout/__Document-root.md}}
{{layout/__Presentation.md}}
{{layout/__Section-wrapper.md}}

Par **Kevin Richard**, le [*20 novembre 2016*](#document-info).

# Analyse du langage non verbal en UX et dans les tests d'utilisabilité
Et si l'on pouvait connaître les émotions d'une personne sans même lui poser la moindre question ? Si vous pouviez avoir un retour clair, objectif, mesurable, quantifiable et reproductible ?

*"Mais comment ? Est-ce de la magie ?"* me diriez-vous. \
Je vous répondrais *"Science, observation et patience"* mes amis.

## Table des matières
1. [Historiques][]
    1. [UX Design & tests d'utilisabilité][]
        1. [Qu'est-ce que l'UX ?][]
        2. [Qu'est-ce qu'un teste d'utilisabilité ?][]
        3. [Utilisation concrète des données en UX Design][]
        4. [L'apport des neuro-sciences & de la psycologie][]
        5. [Le future de l'UX][]
    2. [Communication non verbale][]
        - [Qu'est-ce que l'analyse du langage non verbal ?][]
            1. [Concept][]
            2. [Les principaux courants scientifiques][]
            3. [La méthode Bodysystemics][]
2. [Théorie][]
    1. [Observations][]
    2. [Problèmes & limitations][]
    3. [Idée, concept et objectif][]
    4. [Exemple d'application][]
3. [Expérimentation & recherches][]
    1. [Méthodoligie utilisé][]
    2. [Méthode qualitative][]
    3. [Conditions & recommandations][]
    4. [Matériel d'analyse][]
4. [Conclusion][]
    1. [Amélioration du rapport observateur/observé][]
    2. [Limites & applications][]
    3. [Une richesse insoupçonnée][]
    4. [Qu'est ce que la recherche utilisateur peut amener à la Bodysystemics ?][]
    5. [Comme un lien de parenté][]
5. [Articles connexes][]
6. [Références](#references)

{{layout/__Section-wrapper-close.md}}

<!-- separator -->
<div class="Presentation" style="height: 300px">
    <div class="Overlay Overlay--cover" style="background-image: url('img/photo-1478301672914-6eba52f60d13.jpeg');"></div>
    <div class="Overlay Overlay--transparency Overlay--transparency"></div>
    <div class="Wrapper Wrapper--textCentered Wrapper--textWhite Wrapper--textShadow--high">
        <h1> UX & Non verbale: historiques </h1>
    </div>
</div>

{{layout/__Section-wrapper.md}}

# Historiques
{{partials/__ux-design-user-test.md}}
{{partials/__nonverbal-communication.md}}
{{layout/__Section-wrapper-close.md}}

<!-- separator -->
<div class="Presentation" style="height: 300px">
    <div class="Overlay Overlay--cover" style="background-image: url('img/photo-1453733190371-0a9bedd82893.jpg');"></div>
    <div class="Overlay Overlay--transparency Overlay--transparency"></div>
    <div class="Wrapper Wrapper--textCentered Wrapper--textWhite Wrapper--textShadow--high">
        <h1> Théorie </h1>
    </div>
</div>

{{layout/__Section-wrapper.md}}
{{partials/__theory.md}}
{{layout/__Section-wrapper-close.md}}

<!-- separator -->
<div class="Presentation" style="height: 300px">
    <div class="Overlay Overlay--cover" style="background-image: url('img/photo-1432888498266-38ffec3eaf0a.jpg');"></div>
    <div class="Overlay Overlay--transparency Overlay--transparency"></div>
    <div class="Wrapper Wrapper--textCentered Wrapper--textWhite Wrapper--textShadow--high">
        <h1> Expérimentation </h1>
    </div>
</div>

{{layout/__Section-wrapper.md}}
{{partials/__experimentation.md}}
{{layout/__Section-wrapper-close.md}}

<!-- separator -->
<div class="Presentation" style="height: 300px">
    <div class="Overlay Overlay--cover" style="background-image: url('img/photo-1446776653964-20c1d3a81b06.jpg');"></div>
    <div class="Overlay Overlay--transparency Overlay--transparency"></div>
    <div class="Wrapper Wrapper--textCentered Wrapper--textWhite Wrapper--textShadow--high">
        <h1> Conclusion </h1>
    </div>
</div>

{{layout/__Section-wrapper.md}}
{{partials/__conclusion.md}}
{{layout/__Section-wrapper-close.md}}

<!-- separator -->
{{layout/__Section-wrapper.md}}
{{partials/__connex-articles.md}}
{{partials/__document-info.md}}
{{partials/__references.md}}
{{layout/__Section-wrapper-close.md}}
</div>
</div>