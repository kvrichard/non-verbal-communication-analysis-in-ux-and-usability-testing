## Théorie
### Observations
Comme énoncé [plus haut](#1.quest-cequelux), **D. Norman** (1988) [^dnorman] a établi un lien entre psychologie, design et interface afin de mesurer le retour (feedback) émotionnel de l'utilisateur, voire de l'anticiper [^designeverydaythings]. Dans cette optique, il applique des pratiques dérivées de la recherche des émotions, de l'état d'esprit et de la construction de la pensée logique pour la transposer à la recherche utilisateur [^action]. Aujourd'hui, la neuroscience cognitive s'ajoute aussi comme grille de lecture nouvelle dans l'UX Design [^embodieddesign], répondant à une mutation de la profession lié à l'avènement de la VR & AR [^vrar]. L'idée est de comprendre le comportement et les émotions humaines pour les appréhender comme seule valeur effective du retour utilisateur afin de lui donner la meilleure expérience possible.

#### Problèmes & limitations
Tous les procédés énoncés ont les mêmes limites que les originaux quant à la détection directe d'émotions. À l'heure actuelle, dans la méthode couramment utilisée pour comprendre les ressentis d'un utilisateur, seule la phase dite de "Verbalisation" [^utest1] [^penséevoix] permet de connaître l'état émotionnel de l'utilisateur. Aucune mesure directe ne permet de détecter les émotions : les avancées dans l'imagerie cérébrale ne permettent pas encore de tests en dehors d'un laboratoire, et ni le para verbal ni le non verbal ne sont pris en compte.

Il y a donc un biais, car tout repose sur l'implication et la capacité de verbalisation du sujet. Tout comme un trou noir est détecté par les effets qu'il produit [^gravlens], les émotions sont pour le moment détectées indirectement par divers outils [^paraverbal] [^linguistique].

### Idée, concept et objectif
Dans l'étude du [langage non verbal](#communicationnonverbale), nous sommes capables de détecter et définir une émotion en direct [^facs] [^bsm] [^bsformation].

L'idée est d'utiliser la grille de lecture des émotions du non verbal, afin de donner un nouveau relief aux résultats des tests utilisateur. L'objectif est de limiter les interprétations subjectives, d'optimiser le temps des tests et d'améliorer la compréhension de l'utilisateur. Au final l'observateur aura toujours à poser des questions, mais celles-ci seront plus précises. De plus, cette nouvelle grille de lecture permettra de créer des persona [^persona] plus emphatiques et plus proches de la réalité.

Pour ce faire, la grille doit être la plus simple possible afin d'être rapidement et facilement implémenté dans la procédure existante. Une phase d'étalonnage sera donc probablement nécessaire afin d'atteindre une véritable optimisation.

### Exemple d'application
Voici un exemple de tableau d’un test utilisateur, utilisé afin de voir si le sujet est capable de réaliser les tâches qui lui sont confiées, et de noter tout détails ou commentaires qu'il pourrait relever.

| Participant | Temps | Action / contexte | Verbal | Non verbal |
|-----|-------|--------|------------|--------|
| John Doe | **00:02:34** | Choix entre deux boutons | *"Je ne sais pas lequel cliquer..."* | **Sourcils:** incompréhension |
| John Doe | **00:04:16** | Barre de chargement |  *"Le chargement me paraît long..."* | **Mains:** impatience |
| John Doe | **00:04:33** | Trouver un document | - | **Chaise:** recadrage |

