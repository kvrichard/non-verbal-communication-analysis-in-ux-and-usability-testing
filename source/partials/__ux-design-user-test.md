## UX Design & tests d'utilisabilité
### Qu'est-ce que l'UX ?
L'**Expérience Utilisateur** [^uxp]  (**User Experience** ou **UX** en anglais) est une discipline qui regroupe plusieurs domaines et se réfère à l'expérience totale d'une personne utilisant un produit, un service ou un système. L'objectif est de rendre un système facile d’utilisation, compréhensible immédiatement, ergonomique et logique. Son utilisation doit être intuitive, les réponses doivent être trouvées naturellement et rapidement. En fait il s’agit d’optimiser la conception d’un outil afin qu’il soit le plus efficace et le plus clair que possible. Le design, l’architecture/structure, l’interactivité, les possibilités offertes contribuent à offrir une bonne expérience de l’utilisateur: lorsqu’ils sont optimisés, l’utilisateur ira vite dans son travail, trouvera la réponse qu’il cherche ou le moyen de faire ce qu’il veut aisément, ce qui ne manquera pas de lui plaire. Il continuera, par conséquent, à utiliser le système.

L'idée est apparue à la fin des années 1990 et fût d'abord décrite par **D. Norman** [^dnorman] comme étant les *"actions, réponses et perceptions d’une personne qui résultent de l’usage ou de l’anticipation de l’usage d’un produit, d’un service ou d’un système"* [^designeverydaythings]. On peut en extraire <u>4 principes fondamentaux</u> :

- **L’émotion**
- **La technique** (performance, accessibilité et rapidité de l’interface)
- **L’ergonomie** (utilisabilité)
- **L’analyse**

Depuis, le principe d'*utilisabilité* à été enrichie de nouveaux critères comme l'*efficacité*, *désirabilité* et *crédibilité*.

Il est intéressant de noter que **D. Norman** vient en premier lieu des **sciences neurocognitives**, ce qui explique que l’expérience utilisateur est profondément lié à la <u>psychologie de l’être humain</u>. Le principe fût repris et travaillé comme un processus de création. **J. J. Garrett** [^jjgarett] dans son ouvrage *"The Elements of User Experience"* [^teoue] décomposa ce processus en 5 étapes :

1. **Stratégie**: Définir les attentes des utilisateurs, les objectifs business et ce dont on va parler.
2. **Périmètre**: Délimiter la solution pour qu’elle soit adaptée, c-à-d définir le <u>contexte concret</u>.
3. **Structure**: Organiser et hiérarchiser l’information.
4. **Squelette**: Concevoir les zonings [^zoningwireframes], les wireframes [^zoningwireframes] et définir l’emplacement de chaque élément.
5. **Surface**: Créer la maquette, ajouter la couche graphique.

### Qu'est-ce qu'un teste d'utilisabilité ?
Un teste utilisateur ou d'utilisabilité [^utest1] est une méthode permettant d'évaluer un produit en le faisant tester par des utilisateurs [^utest2]. Il consiste à placer l’utilisateur dans une situation dite « écologique », la plus proche possible de l’utilisation réelle de l’application. L'utilisateur doit suivre des scénarios d’utilisation construits afin de vérifier les hypothèses identifiées précédemment.

L'objectif est de faire ressortir concrètement les véritables difficultés que l'utilisateur rencontre [^ergo], ainsi que les erreurs et points d'amélioration. Le test d'utilisabilité consiste le plus souvent à mesurer quatre grands points :

- **Performance**: combien de temps, et combien d'étapes, sont nécessaires pour terminer différentes tâches simples ? (ex: trouver un produit, créer un compte et acheter le produit)
- **Précision**: combien d'erreurs ont été faites par les utilisateurs test ? Ont-elles été rédhibitoires ou rattrapable avec la bonne information ?
- **Mémoire**: de quels éléments et informations se souvient l'utilisateur après le test ? et après un certain temps sans utilisation ?
- **Réponse émotionnelle**: comment l'utilisateur se sent après la réalisation des tâches ? Est-il confiant ou stressé ? Est-ce que l'utilisateur recommanderait ce produit à un ami ?

Le principe est directement dérivée des méthodes d’expérimentation scientifiques [^plandexp] et vise à mettre en œuvre un protocole permettant de prendre des mesures objectives qui vont servir à valider une ou plusieurs hypothèses énoncées initialement.

### Utilisation concrète des données en UX Design
Au final, grâce à ces différents retours et à l'audite du cheminement des utilisateurs dans un site web ou une application, il possible de créer des **"personas"** [^persona] dont le but est de représenter une catégorie (ou ensemble) d'utilisateurs type. Ces *personas* associé au cheminement précédemment définis, permettent de définir une sorte de carte appelé **"User Journey Map"** [^ujmap] ou **"Customer Journey Map"** [^cjmap] qui permet d'avoir une vue global à la fois de toutes les tâches et processus qu'un utilisateur doit accomplir d'un point **A** à un point **B** mais aussi des éléments bloquant, à améliorer ou à corriger. Cette vue d'ensemble permet d'anticiper, planifier et progressivement améliorer l'expérience utilisateur.


### L'apport des neuro-sciences & de la psycologie
Ces dernières années, le nombre de recherches dans le domaine de l'expérience utilisateur à conscidérablement augmenté. Plusieurs chercheur dans le domaine du cerveau et des neuro-sciences ainsi que des psychologue essai de comprendre notre cerveau lorsqu'il est confronté à une machine. Par exemple, dans [*L'énigme de l'expérience contre la mémoire*](https://www.ted.com/talks/daniel_kahneman_the_riddle_of_experience_vs_memory) **Daniel Kahneman** [^xpvsmemory] (2010), nous explique que le cerveau n'enregistre pas toutes les expériences de notre vie de la même façon. Notament, les expériences négatives ont tendance à rester plus facilement ancrés: le sentiment ou l'émotion négative, même vécu un court moment mais de manière intense, peut alors prendre le dessus sur l'ensemble d'une expérience en réallité positive.

Lors de la création d'interfaces utilisateur, il est important de prendre en compte ces études. La frustration, la colère, le rejet son des émotions hélàs fréquentes lorsqu'on utilise des interfaces. Ces émotions restent par dessus toute l'expérience vécu et peuvent faire fuir à jamais les utilisateurs d'une application.

### Le future de l'UX
Qui à dit qu'il ne fallait pas montrer ces émotions ? Demain elles seront même le nerf de la guerre pour beaucoup d'entreprises. Dans [Designing For The Internet Of Emotional Things](https://www.smashingmagazine.com/2016/04/designing-for-the-internet-of-emotional-things/), **Pamela Pavliscak** (2016) nous explique pourquoi l'industrie technologique vire vers une approche basé sur la detection des émotions afin de répondre au mieux aux besoins utilisateur.
