## Expérimentation & recherches
### Méthodoligie utilisé
Originellement, l'expérience devait se dérouler sur mon lieu de travail à **Swissquote**. Mais pour des raisons d'organisation et de rapidité de mise en place j'ai dû abandonner cette idée pour que le rendu de ce document se fasse dans un temps raisonnable. Cela dit, le projet est toujours d'actualité et je compte bien le mettre en oeuvre dès que possible.
Dans le cadre du rendu de ce travaille de mémoire, il m'a fallu donc trouver une autre solution.

De nos jours, les tests utilisateurs sont souvent filmés, voir opéré de manière distante [^remoteUT] : d'un côté le chercheur créer une liste de tâches basées sur un scénario. De l'autre des utilisateurs "volontaires" essaient de les reproduire depuis le lieu de leurs choix, au moment où ils le souhaitent. Le tout se fait généralement via une plateforme qui met en relation chercheurs/sujets et leur propose des outils adaptés. Et parfois, ces tests sont publiés sur internet (ex. YouTube).

![Exemple de teste utilisateur à distance](img/remote-usertest-example.png)

Je vais donc utiliser ces vidéos "publiques" afin de supporter mon projet. L'idée est d'avoir un maximum de vidéos où seul le sujet (participant) est présent. Mais comme on peut le constater, certaines vidéos peuvent présenter le chercheur et le sujet.

### Méthode qualitative
Le choix de la méthode s'impose à moi étant donné le changement des conditions de test et d'application au cours de mon mémoire. De plus, les laboratoires de recherche, agences et entreprises pratiquant les tests utilisateurs ne publient pas facilement leurs travaux. Les raisons sont diverses, mais souvent liées à des aspects légaux, de sécurité ou de politique interne.

**Pourquoi ce n'est pas un problème ?**

1. Les tests utilisateur et d'utilisabilité sont réalisés par des experts ou des personnes formés.
2. Même sur des phases d'expérimentation ou de recherche, ces tests sont assez standardisés de par le besoin de reproductibilité, de comparaisons et d'échange.
3. De fait, on peut partir du principe qu'ils ont donc tous le même biais au regard du non verbal.

### Conditions & recommandations
1. Dans la mesure du possible, chaque participant doit être filmé de face avec une vue sur l'action ou les tâches à effectuer. Cela permet d'avoir une meilleure compréhension du contexte du test.
2. De même, le son devrait aussi être enregistré afin de pouvoir faire une analyse post-examen.
3. L'observateur et l'analyste post-examen ne doivent pas communiquer avant que ces deux étapes ne soient terminées. Ici les vidéos provenant du web, ce cas ne c'est pas à présenté.
4. Selon l'environnement du test, l'analyse peut être appuyée par du matériel spécifique comme un capteur thermique, des caméras couvrant des angles supplémentaires, etc.

### Matériel d'analyse
Voici une sélection de vidéos utilisés pour mes recherches
{{__usertesting-videos.md}}