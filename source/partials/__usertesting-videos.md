#### Testes utilisateurs filmés
- Tests avec observateur
    + [Video: DownTown App - User Design Testing Session 1](https://www.youtube.com/watch?v=YB4GY_ejGrg)
    + [Video: DownTown App - User Design Testing Session 2](https://www.youtube.com/watch?v=P437jRsavSs)
- Tests à distance
    + [Video: Remote user testing on Virgin America website](https://www.youtube.com/watch?v=VxBc_nhWDUA)
    + [Video: Remote User Testing & Prototypes](https://www.youtube.com/watch?v=-5cCHSuDVNg)
    + [Video: Justinmind Prototyping tool and Userlytics Remote User Testing](https://www.youtube.com/watch?v=GEOsja9FYa4)
    + [Video: Remote User Testing & Software Prototype](https://www.youtube.com/watch?v=kAxXNtd1V8g)
    + [Video: Retail UX: J.Crew Remote User Testing- Product Placement](https://www.youtube.com/watch?v=EyV9DBNr7j0)

#### Chaines Youtube où trouver des testes utilisateurs
- [Page Youtube: RetailUX &rarr;](https://www.youtube.com/user/RetailUX/videos)
- [Page Youtube: JustInMind1 &rarr;](https://www.youtube.com/user/JustInMind1/videos)
