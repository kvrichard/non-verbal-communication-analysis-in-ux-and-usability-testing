## Conclusion
Il apparait que l'analyse du comportement non verbal lors de tests utilisateur peut améliorer sa compréhension initiale. Il est évident que seule l'association de plusieurs méthodes d'analyse simultanées permettra de granulariser et d'affiner la recherche d'expériences utilisateur. Le non verbal en est une relativement facile à mettre en place. Ici point de révolution du domaine, mais l'émergence d'un nouveau spectre de compréhension pour le chercheur.

### Amélioration du rapport observateur/observé
L'analyse de ce type de vidéo reste très riche et apporte beaucoup non seulement quant au participant, mais aussi pour le chercheur/observateur. Apprendre à laisser s'exprimer le participant, lui laisser toute la place tout en le rassurant tout au long du processus de test fait partie du rôle d'observateur. Le risque est toujours de biaiser le test, soit en guidant trop le participant soit en induisant une situation de stress pour ce dernier.

L'expérience vient en pratiquant, mais rien n'empêche l'observateur de s'analyser afin de s'améliorer. Ici une analyse non verbale de la synergie entre participant et observateur ferait sens.

### Limites & applications
Aujourd'hui la recherche utilisateur est devenue essentielle pour assurer le succès et la survie des applications, qui sont de plus en plus présents dans nos vies. Cependant c'est un domaine coûteux, un cadre souvent trop contraignant et beaucoup d'entreprises préfèrent déléguer ce travail à des agences spécialisées.

La demande étant très forte, de nouvelles pratiques émergent [^guerrillatest], éliminant la nécessité d'environnements contrôlés et favorisant les tests de petite envergure, mais répétés. Le milieu urbain devient le laboratoire et dans un tel contexte souvent le matériel d'analyse (de type capteurs, caméras thermiques et logiciels d'analyses vocales) sont mis de côté pour faciliter la mobilité et réduire les coûts.

<small>Exemple de guerrilla testing, ou le "teste utilisateur simplifié"</small> \
<iframe width="560" height="315" src="https://www.youtube.com/embed/0YL0xoSmyZI" frameborder="0" allowfullscreen></iframe>

L'analyse non verbale à toute sa place dans cette discipline, que ce soit dans sa pratique orignale que dans ses variantes & déclinaisons moins couteuses. Cependant, ces dernières laissent peu de place à une analyse approfondie. Leurs buts étant de récolter des informations rapidement, le plus souvent l'analyse vidéo est inexistante. L'observateur doit donc avoir les capacités d'analyse non verbale en temps réel.

### Une richesse insoupçonnée
Pour être honnête, avec un ordinateur comme le médium des interactions entre les participants et l'interface je n'imaginais pas être amené à voir autant d'items. Je pensais même trouver comme un schéma ou une récurrence dans les items observés, comme l'incompréhension, le mépris ou le dépit... éventuellement des démangeaisons au niveau de la mâchoire, etc.

En réalité, c'est tout l'inverse ! Ce qui est rassurant d’un côté (il y a toujours autant de façons d'exprimer une émotion que d'être humain sur la planète) ! Le cerveau ne ferait donc pas la différence entre un ordinateur ou un être humain, entre une expérience sur une interface numérique ou dans la vie [^cerveau] ?

### Qu'est ce que la recherche utilisateur peut amener à la Bodysystemics ?
Au travers de ce document, on a pu voir l'apport de l'analyse du non verbale dans la recherche utilisateur. Mais est-ce transversal ?

La recherche utilisateur applique diverses méthodes et possède un large choix d'outils efficace pour mesurer et analyser le comportement des utilisateurs. Nombreux sont ceux qui peuvent être détournés pour de l'analyse non verbale, notamment les outils d'enregistrement vidéos.

Des méthodes intéressantes existent aussi : par exemple un procédé où le participant est filmé dans une pièce en présence d'un observateur. En plus d'enregistrer la vidéo, elle est diffusée en temps réel sur un écran TV dans une autre pièce. De là, une équipe d'analystes étudient les faits et gestes du participant.

L'idée derrière est d'améliorer la qualité de l'analyse en la confrontant à plusieurs points de vue en même temps. Enfin, l'observateur et les analystes peuvent échanger leurs ressentis juste après la session.

### Comme un lien de parenté
Les deux disciplines ont donc beaucoup à partager, même si elles sont déjà extrêmement liées.
Dans le fond, quel est le point commun entre l'UX, les tests utilisateur et l'analyse du non verbale ? L'être humain, son comportement, sa capacité à interagir avec son environnement, ses émotions et sa capacité d'adaptation sont les points clés qui lient toutes ses disciplines.

Dans un monde plus que jamais numérique, fait de machines, de robots et d'interface, je trouve cela vraiment poétique l’importance de la place de l'être humain dans la conception de ce monde artificiel.