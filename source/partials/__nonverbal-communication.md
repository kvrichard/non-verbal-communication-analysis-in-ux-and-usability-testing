## Communication non verbale
La définition générale de la communication non verbale désigne tout échange n'ayant pas recours à des mots [^nonverbal], c-à-d aussi bien l'intonation de la voix [^paraverbal], que la gestuelle du corps ainsi que les expressions faciales [^expfaciales]. L'idée est que le corps fait passer un message, souvent involontaire, en plus des mots utilisés.

### Qu'est-ce que l'analyse du langage non verbal ?
#### Concept
L'analyse du langage non verbal est le fait d'observer, dans un contexte donné, la gestuelle et les expressions du corps d'un ou plusieurs sujets. Les éléments non verbaux observés - aussi appelé *item* - auxquelles une signification ou un processus est rattaché, peuvent être utilisés afin de décrypter le message que le corps à communiqué.

#### Les principaux courants scientifiques
Voici un bref aperçu des principaux courants scientifiques et des chercheurs qui ont abordé la communication non verbale comme un sujet d’étude à part entière. On distingue principalement deux grands courants de pensée qui caractérisent la science moderne, quelque soit son objet particulier : l’approche **naturaliste** et l’approche **constructiviste** qui peuvent prendre plusieurs formes.

**a) Approches naturalistes ou classiques** \
Ici les émotions - de même que leurs expressions - sont appréhendées comme des objets universaux. Ils sont donc mesurables et doivent être étudiés comme des phénomènes existants en tant que tels. Ils sont pensés comme invariants quelle que soit la culture dans laquelle ils sont étudiés. Ils sont innés, et universellement identiques dans leur forme et leur signification [^univerbale].

Au XIXème siècle, **C. Darwin** et **A. Charma** qui furent les premiers à décrire l’importance des gestes et des expressions faciales comme objets d’étude à part entière, porteurs de significations propres. **C. Darwin** définit et décrit les émotions primaires dans le cadre de sa théorie de l’évolution. Dans cette optique, l’expression émotionnelle est repérable et est parfois commune entre l’homme et l’animal.

**P. Ekman**, universitaire et psychologue, propose une méthode de reconnaissance des émotions appelé le *FACS* [^facs]. Dans sa vision, les émotions sont aussi appréhendées comme universelles et biologiquement déterminées. Bien que largement controversées par les anthropologues et les psychologues sociaux constructivistes, ses théories sont très populaires et font même l’objet d’adaptations télévisuelles comme *"Lie to me"* [^lietome].

**b) Approches constructivistes** \
La pensée constructiviste repose sur l’idée que notre vision du monde et sa réalité perçue sont le résultat de l’interaction entre l’esprit humain et son environnement. Dans cette approche, la réalité du monde est une perception et non la réalité elle-même. La cognition, l’émotion, le langage verbal et non verbal sont le produit d’une culture. L’approche constructiviste n’est pas moins mesurable que l’approche classique. Elle rajoute à l’analyse naturaliste la dimension du contexte qu’il faut prendre en compte afin de comprendre les motivations et les intentions qui sont traduites dans les comportements verbaux et non verbaux. [^paloalto]

**c) Neuroscience cognitive et cognition incarnée** \
Les vingt dernières années ont été très fructueuses en matière de découvertes sur les mécanismes qui régissent la cognition. L’apport des neurosciences et de la neuromimétique en parallèle des nouvelles technologies (EEG, Scanner, IRM fonctionnelle, etc.) ont permis de faire tomber certaines croyances issues des paradigmes naturalistes classiques. Désormais, la pensée, consciente ou non s’inscrit avant tout dans le corps, on parle de cognition incarnée ou d’embodiment [^embodied].

#### La méthode Bodysystemics
La **Bodysystemics** (BS) est issue du constat d’échec et des limites que rencontrèrent certains chercheurs en communication non verbale. En effet, l’homme est nécessairement situé dans une culture et un contexte particuliers dont il s’emprunte. Sa gestuelle est même parfois inversée, par exemple en fonction du sens de l’écriture [^ecriture]. Les gestes et les expressions faciales dans ces conditions ne peuvent pas être exclusivement et strictement universels comme le prétendent les naturalistes, mais dépendent aussi du contexte. Faire un listing exhaustif des items corporels repérés en dehors de la relation qu’entretient le sujet n’a aucun sens et peut même prêter à confusion dans l’interaction. En effet, l’homme ne peut être exclu du système dans lequel il échange.

L’équation qui résume le mieux la BS est : \
`C = f(P,E)` avec `C = comportement`, `P = personne` (gestes, émotions…), `E = environnement` (contexte)

La **méthode Bodysystemics** (BSM) tente de comprendre le système qui articule ces noms ou ces concepts entre eux. Elle intègre donc de facto ce "dictionnaire" mais propose en plus une véritable grammaire afin de mieux comprendre les logiques, les règles et les principes qui sous-tendent la production des gestes, des mimiques et des postures dans chaque contexte particulier [^bsm].

![L'équation BS et le processus de pensée](img/the-behaviour-formula.png)
