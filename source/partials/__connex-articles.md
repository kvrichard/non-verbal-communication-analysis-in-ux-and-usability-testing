## Articles connexes
Voici une liste non-exhaustive de travaux ou d'articles concernant l'état du non verbal dans la recherche UX:

1. [*The Role of Body Language in User Research*](http://blog.keylimeinteractive.com/the-role-of-body-language-in-user-research) (2016) par **M. Bruce** sur [**Key Lime Interactive**](http://keylimeinteractive.com/)
    - Video: [*Let's Face it, Your Emotions Are Showing: Analyzing Facial Responses*](https://www.youtube.com/watch?v=fLcZ27rjQVk) (2016) par **Key Lime Interactive**
    - Video: [*Intro to the Role of Body Language in User Research*](https://www.youtube.com/watch?v=oHtjELDg3J8) (2016) par **Key Lime Interactive**
    - Video: [*The Role of Facial Expressions in User Research*](https://www.youtube.com/watch?v=-O7ltQ4FkOo) (2016) par **Key Lime Interactive**
5. [*Rethinking UX Research: Non-Verbal Clues*](http://www.cooper.com/journal/2016/3/rethinking-ux-research-non-verbal-clues) (2016) par **K. Hill** & **R. P. Zander** sur [**Cooper.com**](http://www.cooper.com/)
6. [*Reading Non-verbal Communication In UX Research*](https://blog.gfk.com/2012/04/reading-non-verbal-communication-in-ux-research/) (2012) par **Anke Schreibe**, publié dans [**GfK Insights Blog**](https://blog.gfk.com/).
7. *[Non-Verbal Communication and UX](http://gamesuserresearchsig.org/wp-content/uploads/2016/05/Adam-Engstroms-GUR-Poster-Large.pdf): How MMO Players Communicate Non-Verbally and How this Affects their Conceptualization of Group Dynamics* (2010) par **Adam Engstrom**, publié dans **The IGDA Games User Research Special Interest Group [(GUR SIG)](http://gamesuserresearchsig.org/)**.
8. [*Influence of Cultural Background on Non-verbal Communication in a Usability Testing Situation*](http://www.ijdesign.org/ojs/index.php/IJDesign/article/viewFile/313/155) (2008) par **P. Yammiyavar**, **T. Clemmensen** & **J. Kumar**, publié dans [**International Journal of Design**](http://www.ijdesign.org/ojs/index.php/IJDesign/issue/view/10).