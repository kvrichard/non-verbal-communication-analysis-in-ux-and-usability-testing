
[^dnorman]: [**Donald Norman**](https://www.wikiwand.com/en/Don_Norman), professeur émérite en sciences cognitives de l'université de Californie.

[^uxp]: [Expérience Utilisateur](https://www.wikiwand.com/en/User_experience), Wikipédia.

[^designeverydaythings]: [*The Design of Everyday Things*](https://www.wikiwand.com/en/The_Design_of_Everyday_Things) (1988) par **Donald Norman**.

[^action]: [Théorie de l'action](https://www.wikiwand.com/fr/Th%C3%A9orie_de_l%27action_(Norman)), D. Norman.

[^jjgarett]: [**Jesse James Garrett**](https://en.wikipedia.org/wiki/Jesse_James_Garrett), architecte de l'information.

[^teoue]: [*The Elements of User Experience*](http://www.jjg.net/elements/pdf/elements.pdf) (2000) par **J. J. Garett.**. <small>Voir aussi [“Jesse James Garett, The Elements of User Experience”](http://www.well.com/conf/inkwell.vue/topics/186/Jesse-James-Garrett-The-Elements-page01.html)</small>

[^zoningwireframes]: Un **Zoning d'interface** et un **wireframe** sont des documents qui schématise l’organisation visuelle d'une interface. [Voir la définition](https://www.wikiwand.com/fr/Wireframe_(design)).

[^utest1]: [Test Utilisateur](https://www.wikiwand.com/fr/Test_utilisateur), Wikipédia.

[^penséevoix]: [Méthode de la pensée à voix haute](https://www.wikiwand.com/fr/M%C3%A9thode_de_la_pens%C3%A9e_%C3%A0_voix_haute), Wikipédia.

[^utest2]: *"Usability Engineering"* (1993) & *"Conception de sites Web : L'art de la simplicité"* (2000) par [**Jakob Nielsen**](https://www.wikiwand.com/fr/Jakob_Nielsen), Nielsen Norman Group.

[^ergo]: [Ergonomie Informatique](https://www.wikiwand.com/fr/Ergonomie_informatique), Wikipédia.

[^plandexp]: [Plan d'expérience](https://www.wikiwand.com/fr/Plan_d%27exp%C3%A9rience), Wikipédia.

[^persona]: [Persona en Ergonomie](https://www.wikiwand.com/fr/Persona_(ergonomie)) ou [Persona en Marketing](https://www.wikiwand.com/fr/Persona_(marketing)), Wikipedia.

[^xpvsmemory]: [User Memory Design: How To Design For Experiences That Last](https://www.smashingmagazine.com/2016/08/user-memory-design-how-to-design-for-experiences-that-last/) par **Curt Arledge** (2016), dans [Smashing Magazine](https://www.smashingmagazine.com)

[^ujmap]: [*User Journeys - The Beginner’s Guide*](http://theuxreview.co.uk/user-journeys-beginners-guide/) (2013) par **C. Mears** publié dans [**TheUXreview**](http://theuxreview.co.uk)

[^cjmap]: [*All You Need To Know About Customer Journey Mapping*](https://www.smashingmagazine.com/2015/01/all-about-customer-journey-mapping/) (2015) par **Paul Boag** publié dans [**Smashing Magazine**](https://www.smashingmagazine.com)

[^nonverbal]: [Communication non verbale](https://www.wikiwand.com/fr/Communication_non_verbale), Wikipédia.

[^paraverbal]: [Le paraverbal](http://www.la-communication-non-verbale.com/2010/02/paraverbal-5471.html), publier dans **La communication non verbale.com**.

[^expfaciales]: [Les Expressions Faciales](https://www.wikiwand.com/fr/Expressions_faciales), Wikipédia.

[^univerbale]: [*L'Expression des émotions chez l'homme et les animaux*](https://www.wikiwand.com/fr/L%27Expression_des_%C3%A9motions_chez_l%27homme_et_les_animaux), **C. Darwin** (1872).

[^facs]: [Facial Action Coding System](https://www.wikiwand.com/en/Facial_Action_Coding_System), **P. Ekman** (1978)

[^lietome]: [*Lie to Me*](https://www.wikiwand.com/en/Lie_to_Me), **Fox Entertainment Group** (2009).

[^paloalto]: [L'École de Palo Alto](https://www.wikiwand.com/fr/%C3%89cole_de_Palo_Alto) (1950), Wikipédia.

[^embodied]: [Embodied Cognition](https://www.wikiwand.com/en/Embodied_cognition) (2011), Wikipédia.

[^ecriture]: [Système d'écriture](https://www.wikiwand.com/fr/Syst%C3%A8me_d'%C3%A9criture), Wikipédia.

[^bsm]: [La Méthode Bodysystemics](http://bodysystemics.ch/methode-bodysystemics), **Institut de Bodysystemics** (2014).

[^bsformation]: [Formation à la méthode Bodysystemics](http://bodysystemics.ch/formations), **Institut de Bodysystemics** (2014).

[^embodieddesign]: [*User Experience as Embodied Experience: Considerations for UX Designers*](https://blog.prototypr.io/user-experience-as-embodied-experience-considerations-for-ux-designers-b66813b08adf#.el7y1mdaa), par [**Lauren Bedal**](https://www.behance.net/lbedal2) (2016)

[^vrar]: [Réalité augmenté (AR)](https://www.wikiwand.com/fr/R%C3%A9alit%C3%A9_augment%C3%A9e) et [Réalité virtuelle (VR)](https://www.wikiwand.com/fr/R%C3%A9alit%C3%A9_virtuelle), Wikipédia.

[^gravlens]: [Lentille gravitationnelle](https://www.wikiwand.com/fr/Lentille_gravitationnelle) décrit dans la [Relativité générale](https://www.wikiwand.com/fr/Relativit%C3%A9_g%C3%A9n%C3%A9rale), **A. Einstein** (1915) - Wikipédia.

[^linguistique]: [*Psychologie cognitive*](https://www.amazon.com/Psychologie-cognitive/dp/2804132218/ref=sr_1_1?s=books&ie=UTF8&qid=1467118505&sr=1-1&keywords=9782804132217), [p. 311](https://books.google.ch/books?id=lL8XzSRFe_MC&pg=PA311&lpg=PA311&dq=psychologie+construction+phrase&source=bl&ots=l6YJSSMy1K&sig=JBq4Y1pjxdfLpHNbf4QPAp5nbsk&hl=en&sa=X&ved=0ahUKEwj3rKWO4crNAhVJvBQKHdmGCRQQ6AEIHDAA#v=onepage&q=psychologie%20construction%20phrase&f=false) par **Patrick Lemaire** (1999), publié aux éditions **De Boeck Supérieur**.

[^remoteUT]: [*What is remote usability testing?*](https://www.usertesting.com/blog/2015/11/30/what-is-remote-usability-testing/), par **Spencer Lanoue** (2015) dans [User Testing Blog, *ideas for a user-friendly world*](https://www.usertesting.com/blog).

[^guerrillatest]: [7 Step Guide to Guerrilla Usability Testing: *DIY Usability Testing Method*](https://userbrain.net/blog/7-step-guide-guerrilla-usability-testing-diy-usability-testing-method), par **Markus Pirker** (2016) dans [Userbrain blog](https://userbrain.net/blog/)

[^cerveau]: [De la vision à l'imagination](http://www.scienceshumaines.com/de-la-vision-a-l-imagination_fr_13502.html) par **Marc Jeannerod** (2003), article issu du [n°43 Hors-série de Sciences Humaines](http://www.scienceshumaines.com/le-monde-de-l-image_fr_147.htm).

*[UX]: User Experience Design
*[c-à-d]: c'est-à-dire
*[FACS]: Facial Action Coding System
*[BS]: Bodysystemics
*[BSM]: Méthode Bodysystemics (ou Bodysystemics Methode)
*[VR]: Virtual Reality (ou Réalité Virtuelle)
*[AR]: Augmented Reality (ou Réalité Agmentée)
