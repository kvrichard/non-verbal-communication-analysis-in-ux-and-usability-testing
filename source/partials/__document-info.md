## Informations & remerciements [document-info]
Ce travail de recherche à été réalisé dans le cadre de mon mémoire de fin d'étude en language non verbale. Il a été commencé le *20 juin 2016* et fût rendu le *20 novembre 2016*. Même si l'idée est peut-être née il y a plus d'un an, courant 2015, il m'aura fallut tout ce temps pour réussir à en  définir clairement les contours et finallement la concrétiser en l'espace de quelques mois.

Je tiens à remercier merci **Rabah Aiouaz** pour ses conseils toujours avisés, et qui à sû nous guider jusqu'au bout de nos projets. Je remercie aussi Yacine Aiouaz, le jury, tous les membres de mon groupe, ainsi que toutes personnes ayant participé à m'aider, d'une manière ou d'une autre, à la réalisation de ce travail de recherche.
